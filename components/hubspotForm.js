import React, {useEffect} from "react";
    
    const HubspotContactForm = props => {
        const { region, portalId, formId } = props;
        useEffect(() => {
            const script = document.createElement('script');
            script.src='//js.hsforms.net/forms/embed/v2.js';
            document.body.appendChild(script);
    
            script.addEventListener('load', () => {
                if (window.hbspt) {
                    window.hbspt.forms.create({
                        region: "na1",
                        portalId: "4832409",
                        formId: "16ae1ed2-9c03-476f-9db5-6ecde25e11b8",
                        target: '#hubspotForm'
                    })
                }
            });
        }, []);
    
        return (
            <div>
                <div id="hubspotForm"></div>
            </div>
        );
    };
    
    export default HubspotContactForm;