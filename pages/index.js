import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '../styles/Home.module.scss'
import Script from 'next/Script'
import HubspotContactForm from '@/components/hubspotForm'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>CareStack Features release webinar 5.30</title>
        <meta charset="utf-8" />
        <meta name="description" content="CareStack Features release webinar 5.30" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="/favicon.ico" />

        {/* <script charset="utf-8" type="text/javaScript" src="//js.hsforms.net/forms/embed/v2.js"></script>
        <script dangerouslySetInnerHTML={{ __html: `
            hbspt.forms.create({
              region: "na1",
              portalId: "4832409",
              formId: "16ae1ed2-9c03-476f-9db5-6ecde25e11b8",
              target: '#hubspot-form-wrapper'
            });
          `}} /> */}
      </Head>
      <style jsx global>{`
        body {
          font-family: ${inter.style.fontFamily};
        }
   
        .hs-error-msgs {
          margin: 0;
          list-style-type: none;
        }
        .hs-error-msgs li {
          line-height: 1;
        }
        .hs-error-msgs .hs-error-msg {
          color: red;
          font-size: 0.75rem;
        }
        .hs-main-font-element {
          color: red;
          font-size: 0.75rem;
        }
        .hs-form-field  {
          margin-bottom: 18px;
          position: relative;
        }
      `}</style>

      {/* spotlight and form */}
      <div className={styles["webinar-spotlight"]}>
			<div className={styles.spotlight__wrapper}>
				<div className={styles["container"]}>
					<div className={styles.spotlight__content}>
						<div className={styles.logo}>
							<Image src="/images/carestack-logo-webinar.svg" alt="carestack logo" width={177} height={22}/>
						</div>
						<h1 className={styles["heading-1"] +" "+ inter.className} >5.30 Feature Release Webinar</h1>
						<p className={styles["p-type1"]}>Welcome to the 5.30 Release Features.  We are excited to announce a brief detail of features coming March 2023.</p>
						<div className={styles.countdown}  id="countdown">
							<div className={styles.countdown__item}>
								<span className={styles.countdown__value} id="days"></span>
								<span className={styles.countdown__label}>DAYS</span>
							</div>
							<div className={styles.countdown__item}>
								<span className={styles.countdown__value} id="hours"></span>
								<span className={styles.countdown__label}>HOURS</span>
							</div>
							<div className={styles.countdown__item}>
								<span className={styles.countdown__value} id="minutes"></span>
								<span className={styles.countdown__label}>MINUTES</span>
							</div>
							<div className={styles.countdown__item}>
								<span className={styles.countdown__value} id="seconds"></span>
								<span className={styles.countdown__label}>SECONDS</span>
							</div>
						</div>
            			<p className={styles["event-started-message"]} id="event-message">Webinar is now live</p>
						<div className={styles.date}>
							<div className={styles.date__desc}>
								<span className={styles.edate}>March 14th, 2023</span>
								<span className={styles.etime}>1 PM ET</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className={styles["hbform-block"]}>
				<div className={styles["hbform-holder"]  +" "+ styles["container"]} id="register-form">
					<div className={styles.hbform}>
						<div className={styles["hbform__wrapper"] +" "+ styles["hbform__wrapper--type2"]}>
							<h2 className={styles["form-header"]}>Register Now!</h2>
							<HubspotContactForm />
						</div>
					</div>
				</div>
			</div>
		</div>
		{/* spotlight and form END*/}

		<div className={styles["content-area"]}>
			{/* topics */}
			<section className={styles.topics}>
				<div className={styles.container}>
					<h2 className={styles["heading-2"]}>Topics of Discussion</h2>
					<div className={styles.topics__wrapper}>
						<div className={styles.topics__item}>
							<div className={styles.topics__image}>
								<Image src="/images/front-desc.png" alt="front desc and clinical" width={456} height={415}/>
							</div>
							<div className={styles.topics__desc}>
								<h3 className={styles["heading-3"]}>Front Desk and Clinical</h3>
								<ul className={styles.topics__list}>
									<li className={styles.topic}>Time Clock Features- edit the Location and filter to view customized period of entries</li>
									<li className={styles.topic}>Recurring Block Slots- Individual or Recurring</li>
									<li className={styles.topic}>Scheduler Setting for Customized Insurance Eligibility Check</li>
									<li className={styles.topic}>No Show or Cancellation addition to add a Code and Fee</li>
									<li className={styles.topic}>Lab Cases- allows you to add a future sent date</li>
								</ul>
								<a className={styles["link-button"]} href="#register-form">Register Now</a>
							</div>
						</div>
						<div className={styles.topics__item}>
							<div className={styles.topics__image}>
								<Image src="/images/reports-and-analytics.png" alt="reporting and analytics" width={456} height={394}/>
							</div>
							<div className={styles.topics__desc}>
								<h3 className={styles["heading-3"]}>Revenue Cycle Management, Patient Engagement, and Reporting and Analytics</h3>
								<ul className={styles.topics__list}>
									<li className={styles.topic}>Medical Alerts- Ability to add without creating a new Medical History</li>
									<li className={styles.topic}>Global Search- Ability to search a patient by Email or last 4 Social Security Number</li>
									<li className={styles.topic}>Online Appointment Updates</li>
									<li className={styles.topic}>Payment plan updates</li>
									<li className={styles.topic}>Statements- New descriptions icon added when selecting &#8220;generate for accounts with&#8221;</li>
									<li className={styles.topic}>New Insurance and Claim Warnings</li>
								</ul>
								<a className={styles["link-button"]} href="#register-form">Register Now</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* topics END*/}

			{/* speakers */}
			<section className={styles.speakers}>
				<div className={styles.container}>
					<h2 className={styles["heading-2"]}>Key Speakers</h2>
					<div className={styles.speakers__wrapper}>
						<div className={styles.speaker}>
							<div className={styles.speaker__image}><Image src="/images/alisha-gerrard.jpg" alt="alisha gerrard" width={325} height={348}/></div>
							<div className={styles.speaker__desc}>
								<span className={styles.speaker__name}>Alisha Gerrard</span>
								<span className={styles.speaker__designation}>Sr. Product Manager</span>
							</div>
						</div>
						<div className={styles.speaker}>
							<div className={styles.speaker__image}><Image src="/images/govind-menon.jpg" alt="govind menon" width={325} height={348}/></div>
							<div className={styles.speaker__desc}>
								<span className={styles.speaker__name}>Govind Menon</span>
								<span className={styles.speaker__designation}>Sr. Product Manager</span>
							</div>
						</div>
						<div className={styles.speaker}>
							<div className={styles.speaker__image}><Image src="/images/jackie-yogerst.jpg" alt="jackie yogerst" width={325} height={348}/></div>
							<div className={styles.speaker__desc}>
								<span className={styles.speaker__name}>Jackie Yogerst</span>
								<span className={styles.speaker__designation}>Sr. Product Manager</span>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* speakers END*/}
		</div>
		<div className={styles["global-footer"]}>
			<div className={styles.container}>
				<span>Copyright &copy; 2023 CareStack. All Rights Reserved.</span>
			</div>
		</div>
		
		<Script id="show-counter" strategy="afterInteractive">
		{`
			var countDownDate = new Date("March 14, 2023 13:00:00").getTime();
			var x = setInterval(function() {
			var now = new Date().getTime();
			var distance = countDownDate - now;
			
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			document.getElementById('days').innerHTML = days;
			document.getElementById('hours').innerHTML = hours;
			document.getElementById('minutes').innerHTML = minutes;
			document.getElementById('seconds').innerHTML = seconds;
				
			// If the count down is over, write some text 
			if (distance < 0) {
				clearInterval(x);
				document.getElementById('countdown').style.display = "none";
				document.getElementById('event-message').style.display = "block";
			}
			}, 1000);

		`}
		</Script>
		<Script id="smooth-scroll" strategy="afterInteractive">
		{`
			//smooth scroll on anchor click
			document.querySelectorAll('a[href^="#"]').forEach(anchor => {
				anchor.addEventListener('click', function (e) {
					e.preventDefault();
					document.querySelector(this.getAttribute('href')).scrollIntoView({
						behavior: 'smooth'
					});
				});
			});
		`}
		</Script>
    </>
    
  )
  
}
